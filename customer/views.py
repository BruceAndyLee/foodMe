from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse

import json
from .models import *

# Create your views here.


def city_layout(request, url_city_name):
    city_name = url_city_name.replace('+', ' ')
    city = City.objects.filter(name=city_name)[0]
    malls = city.shoppingmall_set.all()
    mall_names, mall_addresses = [], []
    for mall in malls:
        mall_names.append(mall.name)

    for mall in malls:
        mall_addresses.append(mall.address)

    mall_dictionary = [{"Name": t, "Address": s} for t, s in zip(mall_names, mall_addresses)]
    return JsonResponse(json.dumps(mall_dictionary), safe=False)


def mall_restaurants(request, url_city_name, url_mall_name):
    mall_name = url_mall_name.replace('+', ' ')
    city_name = url_city_name.replace('+', ' ')
    city = City.objects.filter(name=city_name)[0]
    mall = city.shoppingmall_set.filter(name=mall_name)[0]
    restaurants = mall.restaurant_set.all()
    context = {
        'restaurants': restaurants,
        'mall_name': url_mall_name,
        'city_name': url_city_name
    }

    return render(request, 'customer/mall_restaurants.html', context)


def restaurant_menu(request, url_city_name, url_mall_name, url_rest_name):
    mall_name = url_mall_name.replace('+', ' ')
    city_name = url_city_name.replace('+', ' ')
    rest_name = url_rest_name.replace('+', ' ')
    city = City.objects.filter(name=city_name)[0]
    mall = city.shoppingmall_set.filter(name=mall_name)[0]
    restaurant = mall.restaurant_set.filter(name=rest_name)[0]
    menu_entry_set = restaurant.menu.menuentry_set.all()
    context = {
        'entry_set': menu_entry_set,
        'city': url_city_name,
        'mall': url_mall_name,
        'restaurant': url_rest_name,
    }

    return render(request, 'customer/restaurant_menu.html', context)


def order(request, url_city_name, url_mall_name, url_rest_name):
    mall_name = url_mall_name.replace('+', ' ')
    city_name = url_city_name.replace('+', ' ')
    rest_name = url_rest_name.replace('+', ' ')
    city = City.objects.filter(name=city_name)[0]
    mall = city.shoppingmall_set.filter(name=mall_name)[0]
    restaurant = mall.restaurant_set.filter(name=rest_name)[0]
    menu_entry_set = restaurant.menu.menuentry_set.all()
    print(request.POST)
    received_entries = request.POST.getlist('dish-name')
    print(received_entries)
    for entry in received_entries:
        if menu_entry_set.filter(name=entry).exists():
            continue
        else:
            context = {
                'entry_set': received_entries,
                'error_message': 'unknown item',
                'faulty_entry': entry
            }
            return render(request, 'customer/cart.html', context)

    context = {
        'entry_set': received_entries,
        'city': url_city_name,
        'mall': url_mall_name,
        'rest': url_rest_name
    }
    return render(request, 'customer/cart.html', context)
