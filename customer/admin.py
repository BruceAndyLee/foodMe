from django.contrib import admin

from .models import *

admin.site.register(City)
admin.site.register(ShoppingMall)
admin.site.register(Restaurant)
admin.site.register(Menu)
admin.site.register(MenuEntry)
# Register your models here.
