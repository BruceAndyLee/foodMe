from django.db import models
from datetime import datetime

# Create your models here.


class City(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class ShoppingMall(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def set_address(self, address):
        self.address = address


class Restaurant(models.Model):
    mall = models.ForeignKey(ShoppingMall, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def set_name(self, name):
        self.name = name


class Menu(models.Model):
    restaurant = models.OneToOneField(Restaurant, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return "%s menu" % self.restaurant.name


class MenuEntry(models.Model):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    calories = models.CharField(max_length=10)
    price = models.FloatField(default=1)

    def set_name(self, name):
        self.name = name

    def set_calories(self, calories):
        self.calories = str(calories)

    def set_price(self, price):
        self.price = str(price)

    def __str__(self):
        return self.name


class User(models.Model):
    mobile = models.CharField(max_length=20)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return self.first_name + " " + self.last_name

    def set_first_name(self, first_name):
        self.first_name = first_name

    def set_second_name(self, second_name):
        self.second_name = second_name


class Order(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    menu_entries = models.ManyToManyField(MenuEntry)
    time_of_order = models.DateTimeField(default=datetime.now, blank=True)

    def get_menu_entries(self):
        return self.menu_entries

    def get_total(self):
        return sum([entry.price for entry in self.menu_entries.all()])

    def __str__(self):
        return self.owner.first_name + self.time_of_order


class Cart(models.Model):
    pass