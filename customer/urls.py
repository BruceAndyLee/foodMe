from django.urls import path

from . import views

app_name = 'customer'
urlpatterns = [
    path('<str:url_city_name>/', views.city_layout, name='city_layout'),
    path('<str:url_city_name>/<str:url_mall_name>/', views.mall_restaurants, name='mall_restaurants'),
    path('<str:url_city_name>/<str:url_mall_name>/<str:url_rest_name>/', views.restaurant_menu, name='restaurant_menu'),
    path('<str:url_city_name>/<str:url_mall_name>/<str:url_rest_name>/order/', views.order, name='order'),
]
